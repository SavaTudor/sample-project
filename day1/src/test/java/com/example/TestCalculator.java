package com.example;

import org.junit.Test;

public class TestCalculator {

    @Test
    public void testSum() {
        float a = 5.5f;
        float b = 4.5f;
        String sign = "+";
        try {
            assert (Calculator.execute(a, b, sign) == 10);
        } catch (Exception ex) {
            assert (false);
        }
    }

    @Test
    public void testDif() {
        float a = 5.5f;
        float b = 4.5f;
        String sign = "-";
        try {
            assert (Calculator.execute(a, b, sign) == 1);
        }catch (Exception ex){
            assert (false);
        }
    }

    @Test
    public void testMul() {
        float a = 3.0f;
        float b = 4.0f;
        String sign = "*";
        try {
            assert (Calculator.execute(a, b, sign) == 12);
        }catch (Exception ex){
            assert (false);
        }
    }

    @Test
    public void testDiv() {
        float a = 12.0f;
        float b = 4.0f;
        String sign = "/";
        try {
            assert (Calculator.execute(a, b, sign) == 3);
        }catch (Exception ex){
            assert (false);
        }
        try {
            assert (Calculator.execute(a, 0, sign) == 3);
        }catch (Exception ex){
            assert (true);
        }
    }

    @Test
    public void testInvalidSign(){
        float a = 12.0f;
        float b = 4.0f;
        String sign = "/";
        try {
            assert (Calculator.execute(a, 0, "totallyNotASign") == 3);
        }catch (Exception ex){
            assert (true);
        }
    }
}
