package com.example;

import org.junit.Test;

public class TestOperation {

    @Test
    public void testSum() {
        float a = 5.5f;
        float b = 4.5f;
        try {
            assert (Operation.sum(a, b) == 10);
        } catch (Exception ex) {
            assert (false);
        }
    }

    @Test
    public void testDif() {
        float a = 5.5f;
        float b = 4.5f;
        try {
            assert (Operation.dif(a, b) == 1);
        } catch (Exception ex) {
            assert (false);
        }
    }

    @Test
    public void testMul() {
        float a = 3.0f;
        float b = 4.0f;
        try {
            assert (Operation.mul(a, b) == 12);
        } catch (Exception ex) {
            assert (false);
        }
    }

    @Test
    public void testDiv() {
        float a = 12.0f;
        float b = 4.0f;
        try {
            assert (Operation.div(a, b) == 3);
        } catch (Exception ex) {
            assert (false);
        }
    }
}
