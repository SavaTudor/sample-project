package com.example;

public class Calculator {

    /**
     *
     * @param a float
     * @param b float
     * @param op string which should represent the operator
     * @return a float representing the result of the expression
     * @throws Exception if we try to divide by 0 or the sign is invalid
     */
    public static float execute(float a, float b, String op) throws Exception {
        switch (op.charAt(0)){
            case '+':
                return Operation.sum(a, b);
            case '-':
                return Operation.dif(a, b);
            case '/':
                if(b==0){
                    throw new Exception("Cannot divide by 0");
                }
                return Operation.div(a, b);
            case '*':
                return Operation.mul(a, b);
            default:
                throw new Exception("Invalid sign");
        }
    }
}
