package com.example;

import java.util.Scanner;

public class App {

    /**
     *
     * @param args none
     *
     * in the main class we will initialize a console to read from the keyboard, 2 float variables,
     * the sign of the operation and a continuation flag, which can be "y" or "n". We will initialize the flag with "y"
     *   Then, depending on the value of the flag, if it is "y", we will read a number, then a sign then another number
     *  and we will print the return value of the Calculator.execute function with the given parameters.
     *  If an exception is thrown during the call of the function we will print the message
     */
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        float a, b;
        String sign;
        String contFlag = "y";
        while (contFlag.equals("y")) {
            a = console.nextFloat();
            sign = console.next();
            b = console.nextFloat();
            try {
                System.out.println(Calculator.execute(a, b, sign));
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

            System.out.println("Continue?[y/n]");
            contFlag = console.next();

        }

    }
}
