package com.example;

public class Operation {

    /**
     *
     * @param a float
     * @param b float
     * @return a float representing the sum of the 2 numbers
     */
    public static float sum(float a, float b) {
        return a + b;
    }

    /**
     *
     * @param a float
     * @param b float
     * @return a float representing the difference of the 2 numbers
     */
    public static float dif(float a, float b) {
        return a - b;
    }


    /**
     *
     * @param a float
     * @param b float
     * @return a float representing the multiplication of the 2 numbers
     */
    public static float mul(float a, float b) {
        return a * b;
    }

    /**
     *
     * @param a float
     * @param b float
     * @return a float representing the division of the 2 numbers
     */
    public static float div(float a, float b) {
        return a / b;
    }
}
